import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import analytics from '@react-native-firebase/analytics';
import Icon from 'react-native-vector-icons/Ionicons'
import Login from './Login';
import Home from './Home';
import QrScreen from './QrScan';


const navigate = createNativeStackNavigator();

const MainNavigation = () => {
    const onLogScreenView = async () => {
        try {
            await analytics().logScreenView({
                screen_name: "Home",
                screen_name: "QrScreen",
            })
        } catch (error){
            console.log(error)
        }
    }
    
    useEffect(() => {
        onLogScreenView
    })

    return(
        <NavigationContainer>
            <navigate.Navigator initialRouteName='Login'>
                <navigate.Screen name='Login' component={Login}/>
                <navigate.Screen name='Home' component={TabNavigation}/>
            </navigate.Navigator>
        </NavigationContainer>
    )
}

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
    return(
        <Tab.Navigator>
            <Tab.Screen name='Home' component={Home} options={{headerShown: false, tabBarIcon: () => <Icon name='home' size={26}/>}}/>
            <Tab.Screen name='QrScreen' component={QrScreen} options={{headerShown: false, tabBarIcon: () => <Icon name='qr-code' size={26}/>}}/>
        </Tab.Navigator>
    );
}

export default MainNavigation;
