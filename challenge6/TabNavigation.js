import React from 'react';
import Home from './Home';
import QrScreen from './QrScan';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const navigate = createBottomTabNavigator();

const TabNavigation = () => {
    return(
        <navigate.Navigator initialRouteName='Home'>
            <navigate.Screen name='Home' component={Home}/>
            <navigate.Screen name='QrScreen' component={QrScreen}/>
        </navigate.Navigator>
    );
}
export default TabNavigation;
