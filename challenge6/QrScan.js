import React from "react";
import {Alert} from "react-native";
import {CameraScreen, CameraType} from "react-native-camera-kit";
import { useIsFocused } from "@react-navigation/native";


const QrScreen = ({navigation}) => {
    const isFocused = useIsFocused();
    const onReadCode = (data) => {
        Alert.alert(data.nativeEvent.codeStringValue);
    }
    return(
        isFocused?
        <CameraScreen 
            CameraType={CameraType.back}
            scanBarcode={true}
            onReadCode={(event) => onReadCode(event)}
            showFrame={true}
            frameColor="red"
            laserColor="white"
            />
        : 
        null

    );
}

export default QrScreen;