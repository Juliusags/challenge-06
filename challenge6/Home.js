import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Alert,
  View,
  Button,
  Dimensions,
} from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';

const Home = ({navigation}) => {
  const [position, setPosition] = React.useState({
    latitude: 10,
    longitude: 10,
    latitudeDelta: 0.0001,
    longitudeDelta: 0.0001,
  });

  useEffect(() => {
    crashlytics().log('App Mounted.');
  }, []);


  useEffect(() => {
    Geolocation.getCurrentPosition((pos) => {
      (position) => {
        console.log(position);
        const crd = pos.coords;
        setPosition({
          latitude: crd.latitude,
          longitude: crd.longitude,
          latitudeDelta: 0.0421,
          longitudeDelta: 0.0421,
        });
      }
      (error) => {
        console.log(error);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000}
    })
  }, []);

  const style = StyleSheet.create({
    container: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      alignItems: 'center',
    },
    map: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
     },
  });

  return (
    <SafeAreaView style={style.container}>
      <View>
        <StatusBar barStyle="dark-content" />
        <MapView
      style={style.map}
      provider={PROVIDER_GOOGLE}
      initialRegion={position}
      showsUserLocation={true}
      showsMyLocationButton={true}
      followsUserLocation={true}
      showsCompass={true}
      scrollEnabled={true}
      zoomEnabled={true}
      pitchEnabled={true}
      rotateEnabled={true}>
       <Marker
       title='Yor are here'
      //  description='This is a description'
       coordinate={position}/>
       </MapView>
      </View>
      </SafeAreaView>
  );
};

export default Home;
