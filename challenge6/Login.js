import React, {useEffect, useState} from "react";
import {SafeAreaView, View, Alert, Image, StyleSheet, Dimensions, Button, TextInput} from "react-native";
import { GoogleSignin, GoogleSigninButton, statusCodes } from "@react-native-google-signin/google-signin";
import messaging from '@react-native-firebase/messaging';
import TouchID from "react-native-touch-id";

const Login = ({navigation}) => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [loggedIn, setloggedIn] = useState();

    const optionalConfigObject = {
        title: "Auth is Required",
        color: "#e00606",
        fallbackLabel: "Show Passcode"
    };

    useEffect(() => {
        const unsubscribe = messaging().onMessage(async remoteMessage => {
          Alert.alert('A New FCM Message Arrived!', JSON.stringify(remoteMessage));
        });
        onSetupCloudMessaging();
        getToken();
        return unsubscribe;
      }, []);
    
      const getToken = async () => {
        const token = await messaging().getToken();
        console.log('Token:', token);
      };
    
      const onSetupCloudMessaging = async () => {
        const authStatus = await messaging().requestPermission();
        const enabled =
          authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
          authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    
        if (enabled) {
          console.log('Authorization status:', authStatus);
        }
      };

    useEffect(() => {
        GoogleSignin.configure({
          scopes: ['email'], // what API you want to access on behalf of the user, default is email and profile
          webClientId:
            '691046091284-7aebeeefd1phk3vqqdklg2kgr7lelg9t.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
          offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        });
      }, []);
    
    const pressHandler = () => {
        TouchID.authenticate('Please authenticate yourself', optionalConfigObject)
        .then(success => {
            alert(success)
            signIn()
        })
        .catch(error => {
            alert('Auth Failed')
        });
    }

    const signIn = async () => {
        try {
          await GoogleSignin.hasPlayServices();
          const {accessToken, idToken} = await GoogleSignin.signIn();
          console.log(accessToken, idToken);
          setloggedIn(true);
          navigation.navigate('Home')
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
            alert('Cancel');
          } else if (error.code === statusCodes.IN_PROGRESS) {
            alert('Signin in progress');
            // operation (f.e. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            alert('PLAY_SERVICES_NOT_AVAILABLE');
            // play services not available or outdated
          } else {
            // some other error happened
          }
        }
      };
    
      const style = StyleSheet.create ({
        container: {
            flex: 1,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            alignItems: "center",
        },
        view:{
            flexDirection: 'column',
            marginBottom: 20,
            textAlign: 'center'
        },
        input : {
            alignSelf: 'center',
            borderRadius: 20,
            margin: 5,
            backgroundColor: 'white',
            width: (Dimensions.get('window').width)-(Dimensions.get('window').width/2)+10/100*Dimensions.get('window').width,
            marginBottom: 20
        },
        image:{
            width: 50,
            height: 200,
            alignSelf: 'center',
            resizeMode: 'contain'
        },
        button:{
            paddingBottom: 20
        },
        intent:{
            color: 'black',
            fontWeight: 'bold'
        }
    })
    

    return (
        <SafeAreaView style={style.container}>
            <View>
                <Image style={style.image} source={{uri: "https://cdn-icons.flaticon.com/png/512/2847/premium/2847502.png?token=exp=1652152729~hmac=a53842edb4917d81be98d49b9f8b9614"}}/>
                <TextInput type={email} testID='input-email' style={style.input} value={email} onChangeText={setEmail} placeholder="Username"/>
                <TextInput type={password} testID='input-password' style={style.input} value={password} secureTextEntry={true} onChangeText={setPassword} placeholder="Password"/>   
                <Button testID='login-button' style={style.button} onPress={() => navigation.navigate('Home')} title="Login" color="black"/>
            </View>
            <View>
                <GoogleSigninButton
                style={{width: 192, height: 48, alignItems: "center"}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={() => signIn()}
                />
            </View>
        </SafeAreaView>
    )
}

export default Login;